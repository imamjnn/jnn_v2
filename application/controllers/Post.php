<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Post extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->helper('debug');
		$this->load->library('SiteTheme', '', 'theme');
	}

	public function index($slug=null){
		$this->load->model('Post_model', 'Post');

		$check = $this->Post->getBySlug($slug);
		if(!$check)
			redirect('home');
		$data['post'] = $this->Post->getBySlug($slug);
		//deb($data);
		$this->load->view('web/post/single', $data);
	}

}