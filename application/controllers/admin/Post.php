<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Post extends CI_Controller{
 
	function __construct(){
    	parent::__construct();
    	if(!$this->session->userdata('username')){
			redirect('admin/login');
		}
    	$this->load->helper('debug');
    	$this->load->library('SiteTheme', '', 'theme');
  	}
 
  	public function single($offset=null){
  		$this->load->model('Post_model', 'Post');
		$this->load->library('pagination');
		$next_page = base_url('admin/post');

		$search = $this->input->post('search');
		$config['base_url'] = $next_page;    // url of the page
  		$config['total_rows'] = $this->Post->getCount(); //get total number of records 
  		$config['per_page'] = 6;  // define how many records on page
  		$config['full_tag_open'] = '<ul class="pagination" id="search_page_pagination">';
  		$config['full_tag_close'] = '</ul>';
	  	$config['cur_tag_open'] = '<li class="active"><a href="javascript:void(0)">';
	  	$config['num_tag_open'] = '<li>';
	  	$config['num_tag_close'] = '</li>';
	  	$config['cur_tag_close'] = '</a></li>';
	  	$config['first_link'] = 'First';
	  	$config['first_tag_open'] = '<li>';
	  	$config['first_tag_close'] = '</li>';
	  	$config['last_link'] = 'Last';
	  	$config['last_tag_open'] = '<li>';
	  	$config['last_tag_close'] = '</li>';
	  	$config['next_link'] = FALSE;
	  	$config['next_tag_open'] = '<li>';
	  	$config['next_tag_close'] = '</li>';
	  	$config['prev_link'] = FALSE;
	  	$config['prev_tag_open'] = '<li>';
	  	$config['prev_tag_close'] = '</li>';
	  	$config['page_query_string'] = FALSE;

	  	$this->pagination->initialize($config);

  		$data['posts'] = $this->Post->getAll($search, $config['per_page'], $offset);
  		//deb($data);

    	$this->load->view('admin/post/single', $data);
  	}

  	function create(){
  		$this->load->model('Postcategory_model', 'PCategory');
		$this->load->model('Posttag_model', 'PTag');

		$data['tags'] = $this->PTag->getAll();
		$data['categories'] = $this->PCategory->get();

  		$this->load->view('admin/post/create', $data);
  	}

  	function check_title(){
		$this->load->model('Post_model', 'Post');
		$title = $this->input->post('title');
		$cek = $this->Post->getByCond(['title'=> $title], false, false);
  		if(!$cek){
  			echo '<i style="color: green">Available</i><script>$("#jin").show();</script>';
  		}else{
  			echo '<i style="color: red">Sorry title already exists</i><script>$("#jin").hide();</script>';
  		}
	}

  	function create_proses(){
		$this->load->model('Post_model', 'Post');
		$this->load->model('Posttag_model', 'PTag');
		$id_user = $this->session->userdata('id');

		$data = array(
			'user' => $id_user,
			'title' => $this->input->post('title'),
			'slug' => $this->input->post('slug'),
			'content' => $this->input->post('content'),
			'category' => $this->input->post('category'),
			'main_post' => $this->input->post('main_post'),
			'created' => date('Y-m-d H:i:s'),
			'status' => $this->input->post('status')
			);

		$id_post = $this->Post->insert($data);

		$id_tag = $this->input->post('post_tag');
		foreach($id_tag AS $tag){
		    $tags = array(
			      'post_tag'  => $tag,
			      'post' => $id_post
			     );
		    $this->PTag->insertChain($tags);
		}  
		
		return redirect('admin/post');
	}

	function edit($id){
		$this->load->model('Post_model', 'Post');
		$this->load->model('Postcategory_model', 'PCategory');
		$this->load->model('Posttag_model', 'PTag');

		$check = $this->Post->getById($id);
		if(!$check)
			redirect('admin');

		$data['post'] = $this->Post->getById($id);
		$data['categories'] = $this->PCategory->get();

		$data['tags'] = $this->PTag->getAll();
		$tags = $this->PTag->getCond(['post'=>$id]);
		$view_tag = array();
		foreach($tags as $tag => $value){
		    $view_tag[$value->post_tag] = $value->post_tag;

		}  
		$data['view_tag'] = $view_tag;
		//deb($data);


		$this->load->view('admin/post/edit', $data);
	}

	function edit_proses(){
		$this->load->model('Post_model', 'Post');
		$this->load->model('Posttag_model', 'PTag');
		$id_user = $this->session->userdata('id');

		$data = array(
			'user' => $id_user,
			'title' => $this->input->post('title'),
			'slug' => $this->input->post('slug'),
			'content' => $this->input->post('content'),
			'category' => $this->input->post('category'),
			'main_post' => $this->input->post('main_post'),
			'status' => $this->input->post('status')
			);

		$id_post = $this->input->post('id');
		$cond = array('id'=>$id_post);
		$this->Post->update($cond, $data);

		$this->PTag->remove(['post'=>$id_post]);
		$id_tag = $this->input->post('post_tag');
		foreach($id_tag AS $tag){
		    $tags = array(
			      'post_tag'  => $tag,
			      'post' => $id_post
			     );
		    $this->PTag->replace($tags, ['post' => $id_post]);
		}  

		redirect('admin/post');
	}

	function delpost($id){
		$this->load->model('Post_model', 'Post');
		$this->load->model('Posttag_model', 'PTag');

		$check = $this->Post->getById($id);
		if(!$check)
			redirect('admin');

		$cond = array('post' => $id);

		$this->PTag->remove($cond);
		$this->Post->remove($id);
		redirect('admin/post');
	}

	//CATEGORY
	function category(){
		$this->load->model('Postcategory_model', 'PCategory');

		$data['categories'] = $this->PCategory->get();
		$this->load->view('admin/post/category/single', $data);
	}

	function category_create(){
		$this->load->model('Postcategory_model', 'PCategory');
		$data = array(
			'name' => $this->input->post('name'),
			'slug' => $this->input->post('slug'),
			'created' => date('Y-m-d H:i:s')
			);

		$this->PCategory->insert($data);
		redirect('admin/post/category');
	}

	function delcat($id){
		$this->load->model('Postcategory_model', 'PCategory');

		$this->PCategory->delete($id);
		redirect('admin/post/category');
	}

	//TAG
	function tag(){
		$this->load->model('Posttag_model', 'PTag');

		$data['tags'] = $this->PTag->get();
		$this->load->view('admin/post/tag/single', $data);
	}

	function tag_create(){
		$this->load->model('Posttag_model', 'PTag');
		$data = array(
			'name' => $this->input->post('name'),
			'slug' => $this->input->post('slug'),
			'created' => date('Y-m-d H:i:s')
			);

		$this->PTag->insert($data);
		redirect('admin/post/tag');
	}

	function deltag($id){
		$this->load->model('Posttag_model', 'PTag');

		$this->PTag->delete($id);
		redirect('admin/post/tag');
	}
}