<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Home extends CI_Controller{
 
    function __construct(){
    	  parent::__construct();
        	  if(!$this->session->userdata('username')){
    			  redirect('admin/login');
    		    }
    	      $this->load->library('SiteTheme', '', 'theme');
  	    }
 
  	public function index(){

    	  $this->load->view('admin/home_view');
  	}
}