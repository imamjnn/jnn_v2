<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct(){
    	parent::__construct();
    	$this->load->helper('debug');
    	$this->load->library('SiteTheme', '', 'theme');
  	}

	public function index(){
		$id_user = $this->session->userdata('id');

		if($id_user)
			return redirect('admin');

		$this->load->view('admin/login');
	}

	function login_proses(){
		$this->load->model('User_model', 'User');
		$this->load->model('Userlog_model', 'Ulog');

		$data2 = array(
			'ip' => $this->input->ip_address(),
			'current_login' => date('Y-m-d H:i:s')
			);

		$username = $this->input->post('username');
		$password = md5($this->input->post('password'));

		$cek = $this->User->cek($username, $password);
		if($cek->num_rows() == 1){
			foreach($cek->result() as $data){
				$sess_data['id'] = $data->id;
				$sess_data['username'] = $data->username;
				$this->session->set_userdata($sess_data);
			}
			$this->Ulog->insertData($data2);
			redirect('admin');
		}else{
			$this->session->set_flashdata('message', 'hmmmmmmm!!!!');
			redirect('admin/login');
		}

	}

	function logout(){
		$this->session->sess_destroy();
		redirect('admin/login');
	}
	
}