<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Home extends CI_Controller{
	
	function __construct(){
		parent::__construct();
		$this->load->helper('debug');
		$this->load->library('SiteTheme', '', 'theme');
	}

	public function index(){
		$this->load->model('Postcategory_model', 'PCategory');

		$data['categories'] = $this->PCategory->get();
		//deb($data);
		$this->load->view('web/home', $data);
	}

	function category($slug){
		$this->load->model('Post_model', 'Post');
		$this->load->model('Postcategory_model', 'PCategory');

		$data['page'] = $slug;
		$data['categories'] = $this->PCategory->get();
		$cond = array(
			'category' => $slug,
			'status' => 2
			);
		$data['posts'] = $this->Post->getByCond($cond, false, false);
		//deb($data);

		$this->load->view('web/home_list', $data);

	    
	}

}