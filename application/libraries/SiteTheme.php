<?php

class SiteTheme
{
	private $CI;
	private $theme;

	public function asset_admin($file){
        
        return base_url('template/admin/'.$file);
    }

    public function asset_web($file){
        
        return base_url('template/web/'.$file);
    }
}