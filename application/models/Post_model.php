<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Post_model extends CI_Model{

	function insert($data){
		$this->db->insert('post', $data);
		return $this->db->insert_id();
	}

	function get(){
		$query = $this->db->get('post');

		if($query->num_rows() > 0){
			foreach ($query->result() as $data) {
				$posts[] = $data;
			}
			return $posts;
		}
	}

	function getById($id){
		return $this->db->get_where('post', array('id' => $id))->row();
	}

	function getBySlug($slug){
		return $this->db->get_where('post', array('slug' => $slug))->row();
	}

	function getCount(){
		$query = $this->db->get('post');
		return $query->num_rows();
	}

	function getCountAll($status){
		$this->db->where('status',$status);
		$query = $this->db->get('article');
		return $query->num_rows();
	}

 	function getAll($search, $limit, $offset){
 		if ($search){
		  $this->db->like('title',$search);
		 }     
		 $result['total_rows'] = $this->db->count_all_results('post');
		  
		 if ($search){
		  $this->db->like('title',$search);
		 }
		$this->db->order_by('created', 'DESC'); 
  		$query = $this->db->get('post', $limit, $offset);
  		
  		return $query->result();
 	}

 	function getByCond($cond, $limit, $order){
 		$this->db->where($cond);
 		$this->db->limit($limit);
 		$this->db->order_by('created', $order);
  		$query = $this->db->get('post');
  		
  		return $query->result();
 	}

	function update($id_post, $data){
		$this->db->where($id_post);
		$this->db->update('post', $data);
	}

	function remove($id){
		$this->db->where('id', $id);
		$this->db->delete('post');
		return;
	}
}