<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Posttag_model extends CI_Model{

	function get(){
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get('post_tag');
		if($query->num_rows() > 0){
			foreach ($query->result() as $data) {
				$tags[] = $data;
			}
			return $tags;
		}
	}

	function insertChain($data){
		$this->db->insert('post_tag_chain', $data);
	}

	function insert($data){
		$this->db->insert('post_tag', $data);
	}

	function replace($data, $cond){
		$this->db->insert('post_tag_chain', $data);
		$this->db->where($cond);
	}

	function getCond($cond){
		$this->db->order_by('post_tag.id', 'DESC');
		$this->db->join('post_tag_chain', 'post_tag.id=post_tag_chain.post_tag');
		$this->db->where($cond);
		$query = $this->db->get('post_tag');
		return $query->result();
	}

	function getAll(){
		$query = $this->db->get('post_tag');
		return $query->result();
	}

	function remove($cond){
		$this->db->where($cond);
		$this->db->delete('post_tag_chain');
	}

	function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('post_tag');
		return;
	}

}