<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
* 
*/
class Postcategory_model extends CI_Model{

	function insert($data){
		$this->db->insert('post_category', $data);
	}

	function get(){
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get('post_category');
		if($query->num_rows() > 0){
			foreach ($query->result() as $data) {
				$categories[] = $data;
			}
			return $categories;
		}
	}

	function getLimit(){
		$this->db->order_by('id', 'DESC');
		$this->db->limit(1);
		$query = $this->db->get('post_category');
		if($query->num_rows() > 0){
			foreach ($query->result() as $data) {
				$categories[] = $data;
			}
			return $categories;
		}
	}

	function getById($cat_id){
		return $this->db->get_where('post_category', array('id' => $cat_id))->row();
	}

	function delete($id){
		$this->db->where('id', $id);
		$this->db->delete('post_category');
		return;
	}
}