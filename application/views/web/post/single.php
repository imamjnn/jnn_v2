<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('web/partial/head') ?>
	<link rel="stylesheet" href="<?= $this->theme->asset_web('hlight/styles/googlecode.css') ?>">
	<script src="<?= $this->theme->asset_web('hlight/highlight.pack.js') ?>"></script>
	<script>hljs.initHighlightingOnLoad();</script>
</head>
<body>
	<div class="wrapper">
		<!-- Navbar-->
		<nav id="header" class="navbar navbar-fixed-top">
            <div id="header-container" class="container navbar-container">
                <div class="navbar-header">
                    <a id="brand" class="navbar-brand" href="<?= base_url() ?>">immjnn.com</a>
                </div>
            </div><!-- /.container -->
        </nav>

		<div class="container">
			<h2><?= $post->title ?></h2>
			<hr>
			<?= $post->content ?>
		</div>
	</div>
	<!-- Javascripts-->
	<script src="<?= $this->theme->asset_web('js/jquery.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_web('js/bootstrap.min.js') ?>"></script>
	<script src="<?= $this->theme->asset_web('js/custom.js') ?>"></script>
</body>
</html>