<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('web/partial/head') ?>
	<style>
		ul.cus{
			color: #337ab7;
			font-size: 18px;
			text-shadow: 1px 1px 1px #ddd;
		}
		ul.cus:hover{
			color: #337ab7;
			font-size: 18px;
			text-shadow: 2px 1px 1px #ddd;
		}
	</style>
</head>
<body>
	<div class="wrapper">
		<!-- Navbar-->
		<nav id="header" class="navbar navbar-fixed-top">
            <div id="header-container" class="container navbar-container">
                <div class="navbar-header">
                    <a id="brand" class="navbar-brand" href="<?= base_url() ?>">immjnn.com</a>
                </div>
            </div><!-- /.container -->
        </nav>

		<div class="container">
			<center>
			<?php if($categories): ?>
				<?php foreach($categories as $cat): ?>
				<a style="text-decoration: none;" class="cus-text <?php if($page == $cat->slug){echo'active';}else{echo '';}?>" href="<?= base_url($cat->slug) ?>"> -<?= $cat->name ?>- </a>
				<?php endforeach; ?>
			<?php endif; ?>
			</center>
			<hr>
			<?php if($posts): ?>
				<?php foreach($posts as $post): ?>
				<ul class="cus">
					<li><a style="text-decoration: none;" href="<?= base_url('read/'.$post->slug) ?>"><?= $post->title ?></a></li>
				</ul>	
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
	<!-- Javascripts-->
	<script src="<?= $this->theme->asset_web('js/jquery.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_web('js/bootstrap.min.js') ?>"></script>
	<script src="<?= $this->theme->asset_web('js/custom.js') ?>"></script>
</body>
</html>