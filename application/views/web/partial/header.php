<header class="main-header hidden-print">
	<a class="logo" href="<?= base_url() ?>">immjnn.com</a>
	<nav class="navbar navbar-static-top">

		<!-- Navbar Right Menu-->
		<div class="navbar-custom-menu">
			<ul class="top-nav">
				<!-- User Menu-->
				<li class="dropdown">
					<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button"><i class="fa fa-gamepad fa-lg"></i></a>
				</li>
				<li class="dropdown">
					<a aria-expanded="false" aria-haspopup="true" class="dropdown-toggle" data-toggle="dropdown" href="#" role="button"><i class="fa fa-code fa-lg"></i></a>
				</li>
			</ul>
		</div>
	</nav>
</header>