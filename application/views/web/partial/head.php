<meta charset="utf-8">
<meta content="IE=edge" http-equiv="X-UA-Compatible">
<meta content="width=device-width, initial-scale=1" name="viewport">

<!-- CSS-->
<link href="<?= $this->theme->asset_web('css/bootstrap.min.css')?>" rel="stylesheet" type="text/css">
<link href="<?= $this->theme->asset_web('css/custom.css')?>" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Kaushan+Script|Chewy|Days+One|Cabin+Sketch" rel="stylesheet">

<title>immjnn.com</title>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
<!--if lt IE 9
   script(src='https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')
   script(src='https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js')
-->