<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('admin/partial/head') ?>
</head>
<body class="sidebar-mini fixed">
	<div class="wrapper">
		<!-- Navbar-->
		<?php $this->load->view('admin/partial/header') ?>

		<!-- Side-Nav-->
		<?php $this->load->view('admin/partial/menu') ?>

		<div class="content-wrapper">
			<?php $this->load->view('admin/home_content_view') ?>
		</div>
	</div>
	<!-- Javascripts-->
	<script src="<?= $this->theme->asset_admin('js/jquery-2.1.4.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/essential-plugins.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/bootstrap.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/plugins/pace.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/main.js') ?>"></script> 
</body>
</html>