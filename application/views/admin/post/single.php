<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('admin/partial/head') ?>
</head>
<body class="sidebar-mini fixed">
	<div class="wrapper">
		<!-- Navbar-->
		<?php $this->load->view('admin/partial/header') ?>

		<!-- Side-Nav-->
		<?php $this->load->view('admin/partial/menu') ?>

		<div class="content-wrapper">
			<div class="page-title">
				<div>
					<h1>All Post</h1>
				</div>
				<div>
					<ul class="breadcrumb">
						<li>
							<a class="btn btn-primary" href="<?= base_url('admin/post/create') ?>">Create New</a>
						</li>
					</ul>
				</div>
			</div>
			<div class="row">
			<?php foreach($posts as $post): ?>
			<div class="col-md-6">
			<a href="<?= base_url('admin/post/edit/'.$post->id) ?>">
				<div class="card">
				
					<div class="card-title-w-btn">
						<h3 class="title"><?= $post->title ?></h3>
					</div>
					</a>
					<div class="card-body">
						<i><?= $post->created ?></i>
						<?php if($post->status == 1){ 
							echo '<span class="label label-warning pull-right">Draft</span>';
						}else{
							echo '<span class="label label-info pull-right">Pulish</span>';
						}
						?>
					</div>
				</div>
				
			</div>
			<?php endforeach; ?>
			<div class="col-md-12"><?php echo $this->pagination->create_links(); ?></div>
			</div>
		</div>
	</div>
	<!-- Javascripts-->
	<script src="<?= $this->theme->asset_admin('js/jquery-2.1.4.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/essential-plugins.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/bootstrap.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/plugins/pace.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/main.js') ?>"></script> 
</body>
</html>