<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('admin/partial/head') ?>
</head>
<body class="sidebar-mini fixed">
	<div class="wrapper">
		<!-- Navbar-->
		<?php $this->load->view('admin/partial/header') ?>

		<!-- Side-Nav-->
		<?php $this->load->view('admin/partial/menu') ?>

		<div class="content-wrapper">
			<div class="page-title">
				<div>
					<h1>Create new post</h1>
				</div>
			</div>

			<div class="row">
			<form action="<?= base_url('admin/post/create_proses') ?>" method="post">
				<div class="col-md-9">
					<div class="card">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label class="control-label" for="focusedInput">Title :</label>
									<input style="font-size: 20px" class="form-control" name="title" type="text" id="title" placeholder="Type your title.." onkeyup="createslug()" required="required" autocomplete="off">
									<div id="result"></div> 
									<input type="hidden" class="form-control" name="slug" id="slug" onblur="check_slug_exists()">
								</div>
								<div class="form-group">
									<label class="control-label" for="focusedInput">Content :</label> 
									<textarea class="form-control" name="content" id="editor"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card">
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label class="control-label" for="focusedInput">Category :</label>
									<?php if(!$categories): ?>
									<?php else: ?>
									<div class="animated-radio-button">
										<label><input type="radio" name="category" value="0" checked><span class="label-text">None</span></label>
									</div>
									<?php foreach($categories as $cat): ?> 
									<div class="animated-radio-button">
										<label><input type="radio" name="category" value="<?= $cat->slug ?>"><span class="label-text"><?= $cat->name ?></span></label>
									</div>
									<?php endforeach; ?>
									<?php endif; ?>
									<hr>
									<label class="control-label" for="focusedInput">Tags :</label> 
									<?php if(!$tags): ?>
									<?php else: ?>
									<?php foreach($tags as $tag): ?> 
									<div class="animated-checkbox">
										<label><input type="checkbox" name="post_tag[]" value="<?= $tag->id ?>"><span class="label-text"><?= $tag->name ?></span></label>
									</div>
									<?php endforeach; ?>
									<?php endif; ?>
									<hr>
									<label class="control-label" for="focusedInput">Main Post :</label> 
									<div class="animated-radio-button">
										<label><input id="optionsRadios1" type="radio" name="main_post" value="1" checked=""><span class="label-text">No</span></label>
									</div>
									<div class="animated-radio-button">
										<label><input id="optionsRadios1" type="radio" name="main_post" value="2"><span class="label-text">Yes</span></label>
									</div>
									<hr>
									<div class="form-group">
										<label>Status:</label> 
										<select class="form-control" name="status">
											<option value="1">Draft</option>
											<option value="2">Publish</option>
										</select>
									</div>
									<button class="btn btn-danger" onclick="history.back();">Cancle</button>
									<button class="btn btn-success pull-right" type="submit" id="jin">Save</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>	
			</div>
		</div>
	</div>
	<script>
		function createslug() {
			var title = $('#title').val();
			$('#slug').val(slugify(title));
		}

		function slugify(text) {
			return text.toString().toLowerCase().replace(/\s+/g, '-') // Replace spaces with -
				.replace(/[^\w\-]+/g, '') // Remove all non-word chars
				.replace(/\-\-+/g, '-') // Replace multiple - with single -
				.replace(/^-+/, '') // Trim - from start of text
				.replace(/-+$/, ''); // Trim - from end of text
		}
	</script>
	
	<!-- Javascripts-->
	<script src="<?= $this->theme->asset_admin('ckeditor/ckeditor.js') ?>"></script>
	<script src="<?= $this->theme->asset_admin('ckeditor/js/sample.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/jquery-2.1.4.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/essential-plugins.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/bootstrap.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/plugins/pace.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/main.js') ?>"></script>
	
	<script>
		$(document).ready(function() {
			$("#title").keyup(function() {
				var title = $(this).val();
				if (title.length > 3) {
					$("#result").html('checking...');
					$.ajax({
						type: 'POST',
						url: '<?php echo base_url('admin/post/check_title')?>',
						data: $(this).serialize(),
						success: function(data) {
							$("#result").html(data);
						}
					});
					return false;
				} else {
					$("#result").html('');
				}
			});
		});
	</script>
	<script>
      	initSample();
    </script>
    <script>
		CKEDITOR.config.removePlugins = 'about';
	</script>

</body>
</html>