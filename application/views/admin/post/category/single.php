<!DOCTYPE html>
<html>
<head>
	<?php $this->load->view('admin/partial/head') ?>
</head>
<body class="sidebar-mini fixed">
	<div class="wrapper">
		<!-- Navbar-->
		<?php $this->load->view('admin/partial/header') ?>

		<!-- Side-Nav-->
		<?php $this->load->view('admin/partial/menu') ?>

		<div class="content-wrapper">
			<div class="page-title">
				<div>
					<h1>All Category</h1>
				</div>
			</div>

			<div class="row">
				<div class="col-md-5">
					<div class="card">
						<div class="row">
							<div class="col-lg-12">
							<form action="<?= base_url('admin/post/category_create') ?>" method="post">
								<div class="form-group">
									<label class="control-label" for="focusedInput">Create new category</label>
									<input style="font-size: 20px" class="form-control" name="name" type="text" id="name" placeholder="Type your category name.." onkeyup="createslug()" required="required" autocomplete="off">
									<div id="result"></div> 
									<input type="hidden" class="form-control" name="slug" id="slug" onblur="check_slug_exists()">
								</div>
								<button class="btn btn-success pull-right" type="submit">Save</button>
							</div>
							</form>
						</div>
					</div>
				</div>
				<div class="col-md-7">
				<?php if(!$categories): ?>
				<?php else: ?>
					<?php foreach($categories as $cat): ?>
					<div class="card">
						<div class="card-title-w-btn">
							<h3 class="title"><?= $cat->name ?></h3>
							<div class="card-body">
								<a onclick="return confirm('Are you sure you want to delete?');" href="<?= base_url('admin/post/category_delete/'.$cat->id) ?>"><span class="label label-danger pull-right">Delete</span></a>
							</div>
						</div>
						<div class="card-body">
							<i>Created : <?= $cat->created ?></i>
						</div>
					</div>
					<?php endforeach; ?>
				<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<script>
		function createslug() {
			var name = $('#name').val();
			$('#slug').val(slugify(name));
		}

		function slugify(text) {
			return text.toString().toLowerCase().replace(/\s+/g, '-') // Replace spaces with -
				.replace(/[^\w\-]+/g, '') // Remove all non-word chars
				.replace(/\-\-+/g, '-') // Replace multiple - with single -
				.replace(/^-+/, '') // Trim - from start of text
				.replace(/-+$/, ''); // Trim - from end of text
		}
	</script>
	
	<!-- Javascripts-->
	<script src="<?= $this->theme->asset_admin('js/jquery-2.1.4.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/essential-plugins.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/bootstrap.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/plugins/pace.min.js') ?>"></script> 
	<script src="<?= $this->theme->asset_admin('js/main.js') ?>"></script>
</body>
</html>