<header class="main-header hidden-print">
	<a class="logo" href="<?= base_url('admin') ?>">immjnn.com</a>
	<nav class="navbar navbar-static-top">
		<!-- Sidebar toggle button-->
		<a class="sidebar-toggle" data-toggle="offcanvas" href="#"></a> 

		<!-- Navbar Right Menu-->
		<div class="navbar-custom-menu">
			<ul class="top-nav">
				<!-- User Menu-->
				<li class="dropdown">
					<a href="<?= base_url() ?>" target="_blank" role="button"><i class="fa fa-external-link fa-lg"></i></a>
				</li>
			</ul>
		</div>
	</nav>
</header>