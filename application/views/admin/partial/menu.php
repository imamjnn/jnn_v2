<aside class="main-sidebar hidden-print">
	<section class="sidebar">
		<div class="user-panel">
			<div class="pull-left image"><img alt="User Image" class="img-circle" src="<?= $this->theme->asset_admin('images/user.png') ?>"></div>
			<div class="pull-left info">
				<p>Imam Jinani</p>
				<p class="designation">Junior Developer</p>
			</div>
		</div>

		<!-- Sidebar Menu-->
		<ul class="sidebar-menu">
			<li class="treeview">
				<a href="<?= base_url('admin/post') ?>"><i class="fa fa-edit"></i><span>Post</span></a>
				<ul style="list-style-type: none;">
					<li>
						<a href="<?= base_url('admin/post/category') ?>"><i class="fa fa-circle-o"></i> Category</a>
					</li>
					<li>
						<a href="<?= base_url('admin/post/tag') ?>"><i class="fa fa-circle-o"></i> Tag</a>
					</li>
				</ul>
			</li>
			<li class="treeview">
				<a href=""><i class="fa fa-gear"></i><span>Setting</span></a>
			</li>
			<li class="treeview">
				<a href="<?= base_url('admin/logout') ?>"><i class="fa fa-sign-out"></i><span>Logout</span></a>
			</li>
		</ul>
	</section>
</aside>